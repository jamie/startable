import falcon
import os
import yaml
import io
import csv
import requests
from startable_ethercalc import StartableEthercalc

class Startable:
    config = None

    def __init__(self, config):
        self.config = config 

    def on_post(self, req, resp):
        if self.config["backend"] == "ethercalc":
            b = StartableEthercalc()
            try:
                b.post(req, config)
                out = { "is_error": 0 }
            except Exception as e:
                out = { "is_error": 1, "msg": e.args[0] }
        print(out)
        resp.media = out


config_path = os.getenv('STARTABLE_CONFIG_PATH')

if not config_path:
    config_path = 'config.yml'

config = {}
with open(config_path) as file:
    config = yaml.load(file, Loader=yaml.FullLoader)

s = Startable(config)

api = falcon.API()
api.req_options.auto_parse_form_urlencoded = True 
api.add_route('/api', s )
