import io
import csv
import requests

class StartableEthercalc:
    def post(self, req, config):
        # Collect all the form values into a list.
        values = []
        for field in req.params:
            values.append(req.get_param(field))
        # Create an in-memory file like object because that's what
        # the CSV class needs.
        f = io.StringIO()
        c = csv.writer(f)
        c.writerow(values)
        csv_data = f.getvalue()

        headers = { "Content-Type": "text/csv" }
        r = requests.post(config["url"], headers=headers, data=csv_data)
