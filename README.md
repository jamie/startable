# Startable 

Startable allows you to add forms to any web site and have the submissions
stored via an online spreadsheet or database. 

At this point it is mostly a proof of concept, with critical features not yet
implemented.

## The big picture 

You can use Startable by following these steps:

1. Pick your backend. Well, not much of a choice at this point - only Ethercalc
   is supported (maybe more backends coming soon). So, pick an ethercalc server
   and create a spread sheet on that server.

2.  Launch the backend API service, configured to use your chosen Ethercalc URL.

3. Write out a json formatted file describing the form you want

4. Insert some javascript into your web page that references the json file

5. Monitor your ethercalc sheet!

## The nitty gritty



### Backend

The backend is a [python3 falcon
API](https://falcon.readthedocs.io/en/stable/user/quickstart.html).

To run the backend, you will need to install the `python3-falcon` framework and `gunicorn`. 

Then, copy `config.yml.sample` to `config.yml` and edit `config.yml` to reflect
the URL of your ethercalc server. Be sure to insert an underscore to the
beginning of your ethercalc URL - that's the API address. For example, if your
URL is `https://myethercalc.org/oPu0ohCu`, the API URL would be:
`https://myethercalc.org/_/oPu0ohCu`

And finally:

    gunicorn startable:api

If you want to get nginx to serve content to this backend, you can use the following configuration:

    upstream startable {
      server 127.0.0.1:8000 fail_timeout=0; 
    }
    server {
      listen 0.0.0.0:443;
      root  /path/to/startable/dir/;

      location /api {
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header Host $http_host;
          proxy_redirect off;
          proxy_pass http://startable;
      }
    }


### Front end

The front end is handled purely via javascript using the [alpacajs
framework](http://www.alpacajs.org/index.html).

The `index.html` file is an example that contains all the code you need. You
just need a web server to serve the content.

However, you will need to copy `schemas/individual.json.sample` to `schemas/individual.json` and edit the file to your liking.
